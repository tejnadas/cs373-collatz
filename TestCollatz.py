#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        self.assertEqual(collatz_read("1 10\n"), (1, 10))

    # ----
    # eval
    # ----

    def test_eval_1(self):
        self.assertEqual(collatz_eval((1, 10)), (1, 10, 20))

    def test_eval_2(self):
        self.assertEqual(collatz_eval((100, 200)), (100, 200, 125))

    def test_eval_3(self):
        self.assertEqual(collatz_eval((201, 210)), (201, 210, 89))

    def test_eval_4(self):
        self.assertEqual(collatz_eval((460586, 427698)), (460586, 427698, 400))

    def test_eval_5(self):
        self.assertEqual(collatz_eval((807195, 693108)), (807195, 693108, 504))

    def test_eval_6(self):
        self.assertEqual(collatz_eval((585356, 987697)), (585356, 987697, 525))

    def test_eval_7(self):
        self.assertEqual(collatz_eval((255665, 406122)), (255665, 406122, 441))

    def test_eval_8(self):
        self.assertEqual(collatz_eval((376435, 760570)), (376435, 760570, 509))

    def test_eval_9(self):
        self.assertEqual(collatz_eval((970181, 597490)), (970181, 597490, 525))

    def test_eval_10(self):
        self.assertEqual(collatz_eval((285792, 887973)), (285792, 887973, 525))

    def test_eval_11(self):
        self.assertEqual(collatz_eval((415262, 382211)), (415262, 382211, 449))

    def test_eval_12(self):
        self.assertEqual(collatz_eval((399624, 4380)), (399624, 4380, 443))

    def test_eval_13(self):
        self.assertEqual(collatz_eval((250020, 325483)), (250020, 325483, 407))

    def test_eval_14(self):
        self.assertEqual(collatz_eval((464979, 408358)), (464979, 408358, 449))

    def test_eval_15(self):
        self.assertEqual(collatz_eval((866911, 708291)), (866911, 708291, 525))

    def test_eval_16(self):
        self.assertEqual(collatz_eval((652823, 639645)), (652823, 639645, 416))

    def test_eval_17(self):
        self.assertEqual(collatz_eval((643431, 724398)), (643431, 724398, 504))

    def test_eval_18(self):
        self.assertEqual(collatz_eval((428271, 31963)), (428271, 31963, 449))

    def test_eval_19(self):
        self.assertEqual(collatz_eval((907309, 487122)), (907309, 487122, 525))

    def test_eval_20(self):
        self.assertEqual(collatz_eval((623585, 4233)), (623585, 4233, 470))

    # -----
    # print
    # -----

    def test_print(self):
        sout = StringIO()
        collatz_print(sout, (1, 10, 20))
        self.assertEqual(sout.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        sin = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        sout = StringIO()
        collatz_solve(sin, sout)
        self.assertEqual(
            sout.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
